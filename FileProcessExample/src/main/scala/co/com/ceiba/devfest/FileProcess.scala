package co.com.ceiba.devfest

import org.apache.spark.sql.SparkSession

object FileProcess extends App{
  val spark: SparkSession = SparkSession.builder.appName("LineCount")
    .config("spark.master", "local[*]").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  val textFile = spark.read.textFile("src/main/resources/Readme.md")
  import spark.implicits._
  println("Number of lines: "+textFile.count())
  println("Total words: "+textFile.map(line => line.split(" ").size).reduce((a,b) => a+b))
  println("First line text: "+textFile.first())
  println("Number of words first Line: "+textFile.map(line => line.split(" ").size).first())
  println("Number of words")
  val x = textFile.flatMap(l => l.split("( )")).flatMap(l=> l.split( "[(,)|(.)]"))
  x.groupByKey(x => x.toLowerCase).count().show(200,false)
  spark.stop()
}

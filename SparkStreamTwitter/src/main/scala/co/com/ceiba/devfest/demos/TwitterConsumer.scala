package co.com.ceiba.devfest.demos

import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._

object TwitterConsumer extends App {
    //Configuration Initialization
    val spark: SparkSession = SparkSession.builder.appName("Twitter Stream")
      .config("spark.master", "local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val streamingContext: StreamingContext = new StreamingContext(spark.sparkContext, Seconds(1))
    // Create a DStream from Twitter using the streaming context
    val tweets = TwitterUtils.createStream(streamingContext, None)
    // Now extract the twitter's user and the tweet of each status update into RDD's using map()
    val statuses = tweets.map(status => "USER: @"+status.getUser.getScreenName+" TWEET:  "+status.getText())
    statuses.print()
    streamingContext.start()
    streamingContext.awaitTermination()
}
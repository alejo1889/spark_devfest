package co.com.ceiba.devfest.demos

import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.{SparkConf, _}
import twitter4j.FilterQuery

object TwitterPopularTagsMedellin extends App {
  //Configuration Initialization
  val config: SparkConf = new SparkConf().setAppName("TwitterStream").setMaster("local[*]")
  val sparkContext: SparkContext = new SparkContext(config)
  sparkContext.setLogLevel("ERROR")
  val streamingContext: StreamingContext = new StreamingContext(sparkContext, Seconds(10))
  //Define the filter for the query
  val medellinSouthWest = Array(-75.6, 6.1)
  val medellinNorthEast = Array(-75.5, 6.3)
  val locationsQuery = new FilterQuery().locations(medellinSouthWest, medellinNorthEast)
  // Create a DStream from Twitter using the streaming context
  val tweets = TwitterUtils.createFilteredStream(streamingContext, None, Some(locationsQuery))
  // Now extract the twitter's user and the tweet of each status update into RDD's using map()
  val hashTags = tweets.flatMap(status => status.getText.split(" ").filter(_.startsWith("#")))
  //hashTags.print()
  //---
  val topCounts60 = hashTags.map((_, 1)).reduceByKeyAndWindow(_ + _, Seconds(6000))
    .map{case (topic, count) => (count, topic)}
    .transform(_.sortByKey(false))

  // Print popular hashtags
  topCounts60.foreachRDD(rdd => {
    val topList = rdd.take(20)
    println("\nPopular topics in last 10 minutes (%s total):".format(rdd.count()))
    topList.foreach{case (count, tag) => println("%s (%s tweets)".format(tag, count))}
  })
  //---
  streamingContext.start()
  streamingContext.awaitTermination()
}
package co.com.ceiba.devfest.demos

import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._

object TwitterPopularTags extends App {
  val timeWindow: Int = 600
  val spark: SparkSession = SparkSession.builder.appName("Twitter Stream Popular Tags")
    .config("spark.master", "local[*]").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  val streamingContext: StreamingContext = new StreamingContext(spark.sparkContext, Seconds(10))
  // Create a DStream from Twitter using the streaming context
  val tweets = TwitterUtils.createStream(streamingContext, None)
  // Now extract the twitter's user and the tweet of each status update into RDD's using map()
  val hashTags = tweets.flatMap(status => status.getText.split(" ").filter(_.startsWith("#")))
  val topCount = hashTags.map((_, 1)).reduceByKeyAndWindow(_ + _, Seconds(timeWindow))
      .map{case (topic, count) => (count, topic)}
        .transform(_.sortByKey(false))

  // Print popular hashtags
    topCount.foreachRDD(rdd => {
      val topList = rdd.take(10)
        println(s"\nPopular topics in last ${timeWindow} seconds (%s total):".format(rdd.count()))
        topList.foreach{case (count, tag) => println("%s (%s tweets)".format(tag, count))}
  })

  streamingContext.start()
  streamingContext.awaitTermination()
}
package co.com.ceiba.devfest.demos

import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import twitter4j.FilterQuery

object TwitterFilterByLocation extends App {
  //Configuration Initialization
  val spark: SparkSession = SparkSession.builder.appName("Twitter Stream filter by location")
    .config("spark.master", "local[*]").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  val streamingContext: StreamingContext = new StreamingContext(spark.sparkContext, Seconds(1))
  //Define the filter for the query
  val medellinSouthWest = Array(-75.6, 6.1)
  val medellinNorthEast = Array(-75.5, 6.3)
  val locationsQuery = new FilterQuery().locations(medellinSouthWest, medellinNorthEast)
  // Create a DStream from Twitter with location filter using the streaming context
  val tweets = TwitterUtils.createFilteredStream(streamingContext, None, Some(locationsQuery))
  //print all information about a Tweet
  //tweets.print()
  // Now extract the twitter's user and the tweet of each status update into RDD's using map()
   val statuses = tweets.map(status => "USER: @"+status.getUser.getScreenName+" TWEET:  "+status.getText())
   statuses.print()
   streamingContext.start()
   streamingContext.awaitTermination()
}